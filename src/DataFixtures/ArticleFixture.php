<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use App\Entity\Article;

class ArticleFixture extends Fixture
{
    public function load(ObjectManager $manager)
    {
     for($i = 1; $i <= 10; $i++){
     	$article = new Article();
     	$article->setTitle("Titre de l'article n°$i")
     			->setContent("<p>Contenu de l'article n°$i</p>")
     			->setImage("https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTvmUrErucWQ5t_G-8pIwXIJKXzC19WFu4g6z6wne9Hv7cJFC8L")
     			->setCreatedAt(new \Datetime());

     	$manager->persist($article);

     }

        $manager->flush();
    }
}
